## Set GKE
$ gcloud config set project gus-dev-249919 && \
gcloud config set compute/zone northamerica-northeast1

$ gcloud config get-value project

$ gcloud config set project gus-dev-249919 && \
gcloud config set compute/zone northamerica-northeast1 &&  \
gcloud container clusters get-credentials staging-skypiea-cluster

## Build image and push it to registry                
cd $HOME/Projects/titan_collector/web
docker build -t gcr.io/gus-dev-249919/datavalet/titan_collector:latest -f Dockerfile .
docker push gcr.io/gus-dev-249919/datavalet/titan_collector:latest

## CMD
cd $HOME/Projects/titan_collector/deployments 
$ kubectl apply -f deployment.yaml -n dev
$ k logs airwave-gke-6777d6675c-c66j4 -c airwave-app --previous -n dev
$ k get pod -n dev 
$ k -n dev exec -it airwave-gke-6777d6675c-c66j4 -- /bin/bash

$ kubetail airwave-gke-6777d6675c-vvj5l -n dev

## URL test
https://35.203.24.214/api?type=keygen&user=ADM%2DQC%2DMON%2D99999%2DVC1&password=test123 
34.70.236.217/api?type=keygen&user=ADM%2DQC%2DMON%2D99999%2DVC1&password=test123 

## webtitancloud
http://derek.webtitancloud.com:8080