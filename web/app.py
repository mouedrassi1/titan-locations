import os
from flask import Flask
from flask import escape
from flask import request
import logging
import json
from google.cloud import logging as glogging

# from db_collector import get_locations_test
from exodus import get_customer_by_ddid
from titan_hq import get_user_id, add_or_update_location

client = glogging.Client()
cloud_log = client.logger(__name__)

handler = client.get_default_handler()
cloud_logger = logging.getLogger("cloudLogger")
cloud_logger.setLevel(logging.DEBUG)
cloud_logger.addHandler(handler)


app = Flask(__name__)


@app.route('/')
def hello_world():
    app.config.from_pyfile('config.cfg')
    msg = app.config['MSG']

    db_user = os.getenv("DB_USERFF", app.config['MSG'])

    print("MSG : {0}".format(msg))
    print("db_user : {0}".format(db_user))
    cloud_logger.info("MSG : {0}".format(msg))

    dir_path = os.path.dirname(os.path.realpath(__file__))
    cloud_logger.info("dir_path : {0}".format(dir_path))

    cwd = os.getcwd()
    cloud_logger.info("cwd : {0}".format(cwd))

    target = os.environ.get('TARGET', 'World')
    return 'Hello {}!\n'.format(target)


@app.route('/api')
def api():
    cloud_logger.info("--- START ---")
    types = request.args.get('type', default='*', type=str)
    user = request.args.get('user', default='*', type=str)
    password = request.args.get('password', default='*', type=str)
    public_ip = request.headers.get('X-Forwarded-For', request.remote_addr)

    cloud_logger.info("--- EXODUS GET CUSTOMER CODE ---")
    customer_code = get_customer_by_ddid("STB-ON-WAT-58013-WAP1")
    cloud_logger.info("Customer code : {0}".format(customer_code))

    # cloud_logger.info("--- LOCATIONS GET LOCATIONS ---")
    # locations = get_locations_test()
    # cloud_logger.info("locations : {0}".format(locations))

    cloud_logger.info("--- AUTH TITAN_CLOUD ---")
    cloud_logger.info("CONSUMER_KEY : {0}".format(os.getenv("CONSUMER_KEY", "")))
    cloud_logger.info("CONSUMER_SECRET : {0}".format(os.getenv("CONSUMER_SECRET", "")))
    cloud_logger.info("ACCESS_TOKEN : {0}".format(os.getenv("ACCESS_TOKEN", "")))
    cloud_logger.info("ACCESS_TOKEN_SECRET : {0}".format(os.getenv("ACCESS_TOKEN_SECRET", "")))
    cloud_logger.info("WEB_TITAN_CLOUD_URL : {0}".format(os.getenv("WEB_TITAN_CLOUD_URL", "")))

    user_id = get_user_id(customer_code)

    cloud_logger.info("user_id : {0}".format(user_id))
    cloud_logger.info("user_code : {0}".format(customer_code))
    cloud_logger.info("public_ip : {0}".format(public_ip))

    cloud_logger.info("--- UPDATE LOCATIONS PG DB ---")
    # for test we use user_id = 25
    # user_id = 25  # remove that when TITANHQ FIX.
    # if user_id and customer_code and user and public_ip:
    #     update_db_location(user_id, customer_code, user, public_ip)

    cloud_logger.info("--- UPDATE TITANHQ DB ---")
    if user and customer_code and public_ip:
        add_or_update_location(user_code=customer_code, location_name=user, ip=public_ip)

    result = {'type': escape(types),
              'user': escape(user),
              'password': escape(password),
              'ip': escape(public_ip)}
    json_data = json.dumps(result, indent=2, sort_keys=True)
    cloud_logger.info(json_data)
    cloud_logger.info("--- END ---")
    return json_data


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))
