import requests
import logging
import sys
# from google.cloud import logging as glogging
from requests_oauthlib import OAuth1

consumer_key = "322b3b8792aa758c4533ae90c6fc0d62"
consumer_secret = "3de5193e6a7cd35d2a8c2265427581fd"
access_token = "7d0f2b71572bc9a6538a5ba23bd8474a"
access_token_secret = "6d98d1229f632d358506c9327511559b"
web_titan_cloud_url = "http://derek.webtitancloud.com:8080"

auth = OAuth1(consumer_key,
              consumer_secret,
              access_token,
              access_token_secret)

# client = glogging.Client()
# cloud_log = client.logger(__name__)

# handler = client.get_default_handler()
# cloud_logger = logging.getLogger("cloudLogger")
# cloud_logger.setLevel(logging.DEBUG)
# cloud_logger.addHandler(handler)


def get_list_user_accounts():
    url = "{titan_url}/restapi/users".format(titan_url=web_titan_cloud_url)
    try:
        r = requests.get(url, auth=auth)
        print(r.status_code)
        json_data = r.json()
        if json_data.get('code', '') == 200:
            print(json_data.get('data', ''))
            print(r.json())
            # cloud_logger.info("get_list_user_accounts : {0}".format(json_data.get('data', '')))
            logging.info("get_list_user_accounts : {0}".format(json_data.get('data', '')))
            return json_data.get('data', '')
        else:
            print(json_data.get('code', ''))
            print(r.text)
            # print(json_data.get('error', ''))
    except requests.exceptions.RequestException as e:
        # print("Unexpected error:", sys.exc_info()[0])
        print("Unexpected error:", e)
        # raise
        pass


if __name__ == "__main__":
    get_list_user_accounts()
