$ gcloud config set project training-243319 && \
gcloud config set compute/zone us-central1-f

$ gcloud config get-value project

$ gcloud container clusters create gke-cluster \
                --num-nodes 3 \
                --machine-type n1-standard-1 \
                --zone us-central1-f
                
$ gcloud beta container clusters update gke-cluster \
    --update-addons=Istio=ENABLED --istio-config=auth=MTLS_STRICT

$ kubectl get service -n istio-system

$ gcloud config set project training-243319 && \
gcloud config set compute/zone us-central1-f &&  \
gcloud container clusters get-credentials gke-cluster


## Build image and push it to registry                
cd $HOME/Projects/titan_collector/web
docker build -t gcr.io/gus-dev-249919/datavalet/titan_collector:latest -f Dockerfile .
docker push gcr.io/gus-dev-249919/datavalet/titan_collector:latest

## CMD
cd $HOME/Projects/titan_collector/deployments 
$ kubectl apply -f deployment.yaml 
$ kubetail airwave-gke-8494b7ff5b-v6snt 

## URL test
34.70.236.217/api?type=keygen&user=ADM%2DQC%2DMON%2D99999%2DVC1&password=test123 
k logs airwave-gke-7c46b7479f-7lx8t -c airwave-app --previous -n dev
k get pod -n dev 

## externalTrafficPolicy GCP
$ kubectl -n default patch service airwave --patch '{"spec":{"externalTrafficPolicy":"Local"}}'

## webtitancloud
http://derek.webtitancloud.com:8080