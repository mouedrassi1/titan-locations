import os
import requests
import logging
from google.cloud import logging as glogging
import psycopg2
client = glogging.Client()
cloud_log = client.logger(__name__)

handler = client.get_default_handler()
cloud_logger = logging.getLogger("cloudLogger")
cloud_logger.setLevel(logging.DEBUG)
cloud_logger.addHandler(handler)

exodus_db = os.getenv("EXODUS_DB", "genesis")
exodus_user = os.getenv("EXODUS_USER", "postgres")
exodus_password = os.getenv("EXODUS_PASSWORD", "")
exodus_host = os.getenv("EXODUS_HOST", "localhost")
exodus_port = os.getenv("EXODUS_PORT", "5432")

con = psycopg2.connect(database=exodus_db,
                       user=exodus_user,
                       password=exodus_password,
                       host=exodus_host,
                       port=exodus_port)


def validate_ddid_(payload):
    # payload = {'ip': '192.168.4.4'}
    headers = {'Content-Type': 'application/json'}
    # url = "http://localhost:8080/es/v1/customer/list"
    url = " http://dev-exodus-service/es/v1/customer/list"
    try:
        r = requests.post(url, data=payload, headers=headers)
        cloud_logger.info("request : {0}".format(r.text))
        return r.text
    except requests.exceptions.RequestException as e:
        cloud_logger.error("request exception : {0}".format(e))
        print(e)
        return 0


def validate_ddid(payload):
    auth_token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Imp4Mm81TDVfUENhMnhLU0ltSTJiaCJ9.eyJpc3MiOiJodHRwczovL2R2YW1wLmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1ZWE4NzViMzU0YjE0YzBjMTI2MGU3YmQiLCJhdWQiOlsiaHR0cHM6Ly9kdmFtcC5kYXRhdmFsZXQuZGV2IiwiaHR0cHM6Ly9kdmFtcC5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNTk1ODg0MjE2LCJleHAiOjE1OTU5NzA2MTYsImF6cCI6IklrcVNEemhTZEUzS0pGYUg0QWQ0cmJjbGZRT2pLYU9PIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCJ9.mLa6jYgQ0GC47Ucyt1D3clzZCIT81d7kD5PCvRpQB9WZIFRiSJkq8x9IO4BvxaMK_HA0ULpoR_KNG7PbmZm_G-6qkvvzrcsDod1ftLNnKvIowXpS_N1h4IAryOWgOisBVqTo2-dGsfxlYvKFG01NsJy7KojitXoVPgw34fuWF3JRV1l0FlWZ-C67Vydr3zKhFrcot79JzALTs3zZ41WatJBiWh9v9Cw3Sl3kaPVAJCEQ2xkN32MNiQ7yzoqNTyxd2sst9V504GGDMqVBMmMUvmYBhoiGR656fgNKcKi5PZ7xldIvWW6JOrmAM5QqqREpOvc1hmO98iWMKDwUVLaLxQ","id_token":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Imp4Mm81TDVfUENhMnhLU0ltSTJiaCJ9.eyJuaWNrbmFtZSI6ImRldmVsb3BlcnMiLCJuYW1lIjoiZGV2ZWxvcGVyc0BkYXRhdmFsZXQuY29tIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyLzIzMzkzZWI4ZTJkYmYzNTlkZmYwZTAzODA0YjljZjQ5P3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGZGUucG5nIiwidXBkYXRlZF9hdCI6IjIwMjAtMDctMjdUMjE6MDY6MDEuNTUxWiIsImVtYWlsIjoiZGV2ZWxvcGVyc0BkYXRhdmFsZXQuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOi8vZHZhbXAuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVlYTg3NWIzNTRiMTRjMGMxMjYwZTdiZCIsImF1ZCI6IklrcVNEemhTZEUzS0pGYUg0QWQ0cmJjbGZRT2pLYU9PIiwiaWF0IjoxNTk1ODg0MjE2LCJleHAiOjE1OTU5MjAyMTYsIm5vbmNlIjoiZG5ock16ZzJNRmRXVkhCQlkxWk9SVTlzVXpGSmVXa3lOR2hxTWxkbVVWaHplVmsyWDB3NGJESlBNdz09In0.YIgQZVlq3Sadu-O59LPD_3TFiZSgjGZJkSyKFGdzK9Z_o08202uD2ODCCs50iESRvpi-IeNAPClF5F04MxeD70VqFbwdKXRoiPP7d1toaTK0jIZl-3VvsjeKZ-ai7MJMIVVeNRYSTPHd-lt-fyaokMyfUIYt0ww8Dc0761vFWCeWEuaxBI1j7l1XEYxjGNgSbzeXR8ExPRoFBE9eyUZdnyS1WLAbvj234CVIVUg7GTZA5U1pY9NWq59GC36Hmp2gzONR3u88HPDIR79kJw5vJhuCHbkiNFxnahQs_vcl4RojzaHNeBLPg5_qW7slp8U511sFvwxw4UiZaMG8QU66vg'
    headers = {'Authorization': 'Bearer ' + auth_token}
    url = "http://dev-exodus-service:8080/es/v1/customer/list"
    # headers = {'Content-Type': 'application/json'}
    try:
        r = requests.post(url, data=payload, headers=headers)
        if r.status_code == 200:
            json_data = r.json()
            customer_response = json_data.get("customerResponse", [])
            print(customer_response)
            cloud_logger.info(customer_response)
            return customer_response
        else:
            print("getList error : {0}".format(r.text))
            cloud_logger.error("getList error : {0}".format(r.text))
            return []
    except requests.exceptions.RequestException as e:
        print("getList exception : {0}".format(e))
        cloud_logger.exception("getList exception : {0}".format(e))
        pass
    return []


def getList(payload):
    auth_token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Imp4Mm81TDVfUENhMnhLU0ltSTJiaCJ9.eyJpc3MiOiJodHRwczovL2R2YW1wLmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1ZWE4NzViMzU0YjE0YzBjMTI2MGU3YmQiLCJhdWQiOlsiaHR0cHM6Ly9kdmFtcC5kYXRhdmFsZXQuZGV2IiwiaHR0cHM6Ly9kdmFtcC5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNTk1ODg0MjE2LCJleHAiOjE1OTU5NzA2MTYsImF6cCI6IklrcVNEemhTZEUzS0pGYUg0QWQ0cmJjbGZRT2pLYU9PIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCJ9.mLa6jYgQ0GC47Ucyt1D3clzZCIT81d7kD5PCvRpQB9WZIFRiSJkq8x9IO4BvxaMK_HA0ULpoR_KNG7PbmZm_G-6qkvvzrcsDod1ftLNnKvIowXpS_N1h4IAryOWgOisBVqTo2-dGsfxlYvKFG01NsJy7KojitXoVPgw34fuWF3JRV1l0FlWZ-C67Vydr3zKhFrcot79JzALTs3zZ41WatJBiWh9v9Cw3Sl3kaPVAJCEQ2xkN32MNiQ7yzoqNTyxd2sst9V504GGDMqVBMmMUvmYBhoiGR656fgNKcKi5PZ7xldIvWW6JOrmAM5QqqREpOvc1hmO98iWMKDwUVLaLxQ","id_token":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Imp4Mm81TDVfUENhMnhLU0ltSTJiaCJ9.eyJuaWNrbmFtZSI6ImRldmVsb3BlcnMiLCJuYW1lIjoiZGV2ZWxvcGVyc0BkYXRhdmFsZXQuY29tIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyLzIzMzkzZWI4ZTJkYmYzNTlkZmYwZTAzODA0YjljZjQ5P3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGZGUucG5nIiwidXBkYXRlZF9hdCI6IjIwMjAtMDctMjdUMjE6MDY6MDEuNTUxWiIsImVtYWlsIjoiZGV2ZWxvcGVyc0BkYXRhdmFsZXQuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOi8vZHZhbXAuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVlYTg3NWIzNTRiMTRjMGMxMjYwZTdiZCIsImF1ZCI6IklrcVNEemhTZEUzS0pGYUg0QWQ0cmJjbGZRT2pLYU9PIiwiaWF0IjoxNTk1ODg0MjE2LCJleHAiOjE1OTU5MjAyMTYsIm5vbmNlIjoiZG5ock16ZzJNRmRXVkhCQlkxWk9SVTlzVXpGSmVXa3lOR2hxTWxkbVVWaHplVmsyWDB3NGJESlBNdz09In0.YIgQZVlq3Sadu-O59LPD_3TFiZSgjGZJkSyKFGdzK9Z_o08202uD2ODCCs50iESRvpi-IeNAPClF5F04MxeD70VqFbwdKXRoiPP7d1toaTK0jIZl-3VvsjeKZ-ai7MJMIVVeNRYSTPHd-lt-fyaokMyfUIYt0ww8Dc0761vFWCeWEuaxBI1j7l1XEYxjGNgSbzeXR8ExPRoFBE9eyUZdnyS1WLAbvj234CVIVUg7GTZA5U1pY9NWq59GC36Hmp2gzONR3u88HPDIR79kJw5vJhuCHbkiNFxnahQs_vcl4RojzaHNeBLPg5_qW7slp8U511sFvwxw4UiZaMG8QU66vg'
    headers = {'Authorization': 'Bearer ' + auth_token}
    url = "https://portal-dev-northamerica-northeast1-svc.datavalet.dev/es/v1/customer/list"
    try:
        r = requests.post(url, data=payload, headers=headers)
        if r.status_code == 200:
            json_data = r.json()
            customer_response = json_data.get("customerResponse", [])
            print(customer_response)
            cloud_logger.info(customer_response)
            return customer_response
        else:
            print("getList error : {0}".format(r.text))
            cloud_logger.error("getList error : {0}".format(r.text))
            return []
    except requests.exceptions.RequestException as e:
        print("getList exception : {0}".format(e))
        cloud_logger.exception("getList exception : {0}".format(e))
        pass
    return []


def get_customer_by_ddid(ddid):
    sql = '''select coalesce((select query.customer_code  as customerCode from (
            SELECT table_site.id,
            table_hardware.ddid AS hardware_ddid,
            table_site.customer_code,
            table_site.customer_name,
            table_site.sitestatus
            FROM ( SELECT site.id,
            site.customer_id,
            ( SELECT customer.code
                   FROM site.customer
                  WHERE customer.id = site.customer_id) AS customer_code,
            ( SELECT customer.name
                   FROM site.customer
                  WHERE customer.id = site.customer_id) AS customer_name,
            ( SELECT sitestatus.name
                   FROM site.sitestatus
                  WHERE sitestatus.id = site.sitestatus_id) AS sitestatus
            FROM site.site) table_site
            LEFT JOIN ( SELECT hardware.id,
             hardware.site_id,
            ( SELECT hardwareattribute.attributevalue
                   FROM site.hardwareattribute
                  WHERE hardwareattribute.hardware_id = hardware.id AND hardwareattribute.hardwareattributetype_id = 3) AS ddid
            FROM site.hardware) table_hardware ON table_site.id = table_hardware.site_id
            LEFT JOIN site.hardwaremac table_hardwaremac ON table_hardware.id = table_hardwaremac.hardware_id
            ) query where query.hardware_ddid = '{0}'), '') as customerCode'''.format(ddid)
    try:
        cur = con.cursor()
        print("Database opened successfully")
        cloud_logger.info("Database opened successfully")
        cur.execute(sql)
        row = cur.fetchone()
        print("Operation done successfully")
        cloud_logger.info("Operation done successfully")
        if row:
            print("customer code : {0}".format(row[0]))
            cloud_logger.info("customer code : {0}".format(row[0]))
            return row[0]
    except psycopg2.Error as e:
        print("get_customer_by_ddid exception : {0}".format(e))
        cloud_logger.exception("get_customer_by_ddid exception : {0}".format(e))
    finally:
        con.close()
    return ""


if __name__ == "__main__":
    # getList({})
    get_customer_by_ddid("STB-ON-WAT-58013-WAP1")
