import requests
import os
import logging
from requests_oauthlib import OAuth1
from google.cloud import logging as glogging
client = glogging.Client()
cloud_log = client.logger(__name__)

handler = client.get_default_handler()
cloud_logger = logging.getLogger("cloudLogger")
cloud_logger.setLevel(logging.DEBUG)
cloud_logger.addHandler(handler)

# consumer_key = "322b3b8792aa758c4533ae90c6fc0d62"
# consumer_secret = "3de5193e6a7cd35d2a8c2265427581fd"
# access_token = "7d0f2b71572bc9a6538a5ba23bd8474a"
# access_token_secret = "6d98d1229f632d358506c9327511559b"
# web_titan_cloud_url = "http://derek.webtitancloud.com:8080"

consumer_key = os.getenv("CONSUMER_KEY", "322b3b8792aa758c4533ae90c6fc0d62")
consumer_secret = os.getenv("CONSUMER_SECRET", "3de5193e6a7cd35d2a8c2265427581fd")
access_token = os.getenv("ACCESS_TOKEN", "7d0f2b71572bc9a6538a5ba23bd8474a")
access_token_secret = os.getenv("ACCESS_TOKEN_SECRET", "6d98d1229f632d358506c9327511559b")
# web_titan_cloud_url = os.getenv("WEB_TITAN_CLOUD_URL", "http://derek.webtitancloud.com:8080")
web_titan_cloud_url = os.getenv("WEB_TITAN_CLOUD_URL", "http://34.95.41.124")

auth = OAuth1(consumer_key,
              consumer_secret,
              access_token,
              access_token_secret)


def get_user_id(user_code):
    list_user = get_list_user_accounts()
    user = next((item for item in list_user if item["account_name"] == user_code), None)
    if user:
        print(user.get("id", ""))
        print(user)
        return user.get("id", "")
    return ""


def get_location_id(user_id, location_name):
    locations = get_locations(user_id)
    location = next((item for item in locations if item["name"] == location_name), None)
    print("location : ", location)
    if location:
        print("location : ", location.get("id", ""))
        return location.get("id", "")
    return ""


def get_list_user_accounts():
    url = "{titan_url}/restapi/users" \
        .format(titan_url=web_titan_cloud_url)
    try:
        r = requests.get(url, auth=auth)
        if r.status_code == 200:
            json_data = r.json()
            print("get_list_user_accounts : {0}".format(json_data.get('data', '')))
            cloud_logger.info("get_list_user_accounts : {0}".format(json_data.get('data', '')))
            return json_data.get('data', '')
        else:
            print("get_list_user_accounts error : {0}".format(r.text))
            cloud_logger.error("get_list_user_accounts error : {0}".format(r.text))
            return []
    except requests.exceptions.RequestException as e:
        print("get_list_user_accounts exception : {0}".format(e))
        cloud_logger.exception("get_list_user_accounts exception : {0}".format(e))
        pass
    return []


def get_user_account(user_id):
    url = "{titan_url}/restapi/users/{user_id}" \
        .format(titan_url=web_titan_cloud_url, user_id=user_id)
    try:
        r = requests.get(url, auth=auth)
        if r.status_code == 200:
            json_data = r.json()
            print("get_user_account : {0}".format(json_data))
            cloud_logger.info("get_user_account : {0}".format(json_data))
            return json_data
        else:
            print("get_user_account error : {0}".format(r.text))
            cloud_logger.error("get_user_account error : {0}".format(r.text))
            return ""
    except requests.exceptions.RequestException as e:
        print("get_user_account exception : {0}".format(e))
        cloud_logger.exception("get_user_account exception : {0}".format(e))
        pass
    return ""


def delete_location(user_id, location_id):
    url = "{titan_url}/restapi/users/{user_id}/locations/dynamicip/{locationid}" \
        .format(titan_url=web_titan_cloud_url, user_id=user_id, locationid=location_id)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    try:
        r = requests.delete(url, headers=headers, auth=auth)
        if r.status_code == 200:
            json_data = r.json()
            print(json_data)
            print("delete location {0} successfully".format(json_data))
            cloud_logger.info("delete location {0} successfully".format(json_data))
            return True
        else:
            print("delete_location error : {0}".format(r.text))
            cloud_logger.error("delete_location error : {0}".format(r.text))
            return False
    except requests.exceptions.RequestException as e:
        print("delete_location exception : {0}".format(e))
        cloud_logger.exception("delete_location exception : {0}".format(e))
        pass
    return False


def get_locations(user_id):
    url = "{titan_url}/restapi/users/{user_id}/locations/dynamicip" \
        .format(titan_url=web_titan_cloud_url, user_id=user_id)
    try:
        r = requests.get(url, auth=auth)
        if r.status_code == 200:
            json_data = r.json()
            print(json_data.get('data', ''))
            cloud_logger.info(json_data.get('data', ''))
            return json_data.get('data', '')
        else:
            print("get_locations error : {0}".format(r.text))
            cloud_logger.error("get_locations error : {0}".format(r.text))
            return ""
    except requests.exceptions.RequestException as e:
        print("get_locations exception : {0}".format(e))
        cloud_logger.exception("get_locations exception : {0}".format(e))
        pass
    return ""


def update_location(user_id, location_id, payload):
    # payload = {'ip': '192.168.4.4'}
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    url = "{titan_url}/restapi/users/{user_id}/locations/dynamicip/{locationid}" \
        .format(titan_url=web_titan_cloud_url, user_id=user_id, locationid=location_id)
    try:
        r = requests.post(url, data=payload, headers=headers, auth=auth)
        if r.status_code == 200:
            json_data = r.json()
            print("update location {0} successfully".format(json_data.get("id", "")))
            cloud_logger.info("update location {0} successfully".format(json_data.get("id", "")))
            return json_data.get("id", "")
        else:
            print("update_location error : {0}".format(r.text))
            cloud_logger.error("update_location error : {0}".format(r.text))
            return 0
    except requests.exceptions.RequestException as e:
        print("update_location exception : {0}".format(e))
        cloud_logger.exception("update_location exception : {0}".format(e))
        pass
    return 0


def add_location(user_id, payload):
    # payload = {'ip': '192.168.5.5', 'name': 'test12345678'}
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    url = "{titan_url}/restapi/users/{user_id}/locations/dynamicip" \
        .format(titan_url=web_titan_cloud_url, user_id=user_id)

    try:
        r = requests.post(url, data=payload, headers=headers, auth=auth)
        if r.status_code == 200:
            json_data = r.json()
            print("add location {0} successfully".format(json_data.get("id", "")))
            cloud_logger.info("add location {0} successfully".format(json_data.get("id", "")))
            return json_data.get("id", "")
        else:
            print("add_location error : {0}".format(r.text))
            cloud_logger.error("add_location error : {0}".format(r.text))
            return 0
    except requests.exceptions.RequestException as e:
        print("add_location exception : {0}".format(e))
        cloud_logger.exception("add_location exception : {0}".format(e))
        pass
    return 0


def add_or_update_location(user_code, location_name, ip):
    user_id = get_user_id(user_code)
    if not user_id:
        return 0
    location_id = get_location_id(user_id, location_name)
    if location_id:
        payload = {'ip': ip}
        location_id = update_location(user_id, location_id, payload)
        return location_id
    else:
        payload = {'ip': ip, 'name': location_name}
        location_id = add_location(user_id, payload)
        return location_id
    return 0


if __name__ == "__main__":
    get_list_user_accounts()
    # get_user_id('ABC')
    # get_user_id('ADM')
    # get_user_account(user_id=25)
    # data = get_locations(user_id=25)
    # print(data)
    # for ele in data:
    #     print(ele.get("name", ""), ele.get("ip", ""), ele.get("id", ""))
    #
    # add_or_update_location("ADM", "ADM-QC-MON-99999-VC1", "127.0.0.4")
    # get_location_id("25", "location_name")

    # get_location_id(25, "Instant-C9:86:D4")
    # update_location(user_id=25, location_id=2693, payload={'ip': '192.168.4.7'})
    # add_location(user_id=25, payload={'ip': '192.168.10.5', 'name': 'abc_test'})
    # delete_location(user_id=25, location_id=2699)


