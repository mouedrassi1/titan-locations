-- create table location

CREATE DATABASE locations
    WITH
    OWNER = cloudsqlsuperuser
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF8'
    LC_CTYPE = 'en_US.UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

select * from locations;

UPDATE public.locations
	SET ddid='ddid_update1'
	WHERE id = 1;

CREATE TABLE IF NOT EXISTS locations(
	id SERIAL PRIMARY KEY,
	user_id integer NOT NULL,
	user_code VARCHAR (3) NOT NULL,
	ddid VARCHAR (50) UNIQUE NOT NULL,
	ip VARCHAR (50) UNIQUE NOT NULL,
	create_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	update_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	);

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.update_on = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS set_timestamp ON locations CASCADE;

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON locations
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();


INSERT INTO public.locations (user_id, user_code, ddid, ip)
                VALUES('{user_id}','{user_code}','{ddid}','{ip}')
                ON CONFLICT (ddid) DO UPDATE SET ip = '{ip}';

INSERT INTO public.locations (user_id, user_code, ddid, ip)
                VALUES(25,'ADM','ADM-ON-WAT-58013-WAP1','217.1.0.12')
                ON CONFLICT (ddid) DO UPDATE SET ip = '217.1.0.12';
