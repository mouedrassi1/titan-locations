import psycopg2
import os
import logging
from google.cloud import logging as glogging
client = glogging.Client()
cloud_log = client.logger(__name__)
handler = client.get_default_handler()
cloud_logger = logging.getLogger("cloudLogger")
cloud_logger.setLevel(logging.DEBUG)
cloud_logger.addHandler(handler)


# LOCATIONS_DB: locations
# LOCATIONS_USER: postgres
# LOCATIONS_PASSWORD: pLil3658FHzPKGC5
# LOCATIONS_HOST: 10.80.0.3
# LOCATIONS_PORT: "5432"
# PGPASSWORD=6OrF9iFxA53NvC1m psql -U postgres -h 34.95.48.158 -p 5432 locations
locations_db = os.getenv("LOCATIONS_DB", "locations")
locations_user = os.getenv("LOCATIONS_USER", "postgres")
locations_password = os.getenv("LOCATIONS_PASSWORD", "6OrF9iFxA53NvC1m")
locations_host = os.getenv("LOCATIONS_HOST", "34.95.48.158")
locations_port = os.getenv("LOCATIONS_PORT", "5432")


# locations_db = os.getenv("LOCATIONS_DB", "locations")
# locations_user = os.getenv("LOCATIONS_USER", "postgres")
# locations_password = os.getenv("LOCATIONS_PASSWORD", "")
# locations_host = os.getenv("LOCATIONS_HOST", "localhost")
# locations_port = os.getenv("LOCATIONS_PORT", "5432")

con = psycopg2.connect(database=locations_db,
                       user=locations_user,
                       password=locations_password,
                       host=locations_host,
                       port=locations_port)


def update_db_location(user_id, user_code, ddid, ip):
    print("Database opened successfully")
    cur = con.cursor()
    cur.execute('''
                INSERT INTO public.locations (user_id, user_code, ddid, ip)
                VALUES('{user_id}','{user_code}','{ddid}','{ip}') 
                ON CONFLICT (ddid) DO UPDATE SET ip = '{ip}'; 
                '''.format(user_id=user_id, user_code=user_code, ddid=ddid, ip=ip))

    con.commit()
    print("Record inserted successfully")
    # con.close()


def update_db_location_(user_id, user_code, ddid, ip):
    print("Database opened successfully")
    cur = con.cursor()
    cur.execute('''
                INSERT INTO public.locations (user_id, user_code, ddid, ip)
                VALUES('{user_id}','{user_code}','{ddid}','{ip}') 
                ON CONFLICT (ddid) DO UPDATE SET ip = '{ip}'; 
                '''.format(user_id=user_id, user_code=user_code, ddid=ddid, ip=ip))

    con.commit()
    print("Record inserted successfully")
    # con.close()


def update_db_location(user_id, user_code, ddid, ip):
    sql = '''
    INSERT INTO locations (user_id, user_code, ddid, ip)
    VALUES('{user_id}','{user_code}','{ddid}','{ip}') 
    ON CONFLICT (ddid) DO UPDATE SET ip = '{ip}'; 
    '''.format(user_id=user_id, user_code=user_code, ddid=ddid, ip=ip)
    try:
        cur = con.cursor()
        print("Database opened successfully")
        cloud_logger.info("Database opened successfully")
        cur.execute(sql)
        print("Update locations successfully")
        cloud_logger.info("Update locations successfully")
    except psycopg2.Error as e:
        print("update_db_location exception : {0}".format(e))
        cloud_logger.exception("update_db_location exception : {0}".format(e))
    finally:
        # con.close()
        pass
    return ""


def get_locations_test():
    sql = '''select * from locations'''
    try:
        cur = con.cursor()
        print("Database opened successfully")
        cloud_logger.info("Database opened successfully")
        cur.execute(sql)
        # row = cur.fetchone()
        rows = cur.fetchall()
        print("Operation done successfully")
        cloud_logger.info("Operation done successfully")
        for index, row in enumerate(rows):
            print("location {0}: {1}".format(index, row))
            cloud_logger.info("location {0}: {1}".format(index, row))
    except psycopg2.Error as e:
        print("get_locations_test exception : {0}".format(e))
        cloud_logger.exception("get_locations_test exception : {0}".format(e))
    finally:
        # con.close()
        pass
    return ""

if __name__ == "__main__":
    # update_db_location(user_id=25, user_code="ADM", ddid="ddid_122341", ip="192.168.0.0")
    get_locations_test()
    # update_db_location(user_id=25, user_code="ADM", ddid="ddid_122341", ip="192.168.11.6")
    # get_db_locations()
    # con.close()